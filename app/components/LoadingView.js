import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';


export default class LoadingView extends Component<Props> {
    render() {
        return(
            <View style={{flex: 1, textAlign: 'center', justifyContent: 'center', alignItems: 'center',}}>
                <Text>Loading...</Text>
            </View>
        );
    }
}