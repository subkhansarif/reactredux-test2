import React, { Component } from 'react';
import { connect } from 'react-redux';
import { watchMsgData, actionSetReadStatus } from '../../react-redux/app-redux';
import {
    StyleSheet,
    Text,
    FlatList,
    View,
    TouchableWithoutFeedback,
    AsyncStorage,
} from 'react-native';


const styles = StyleSheet.create({
    containerLight: {
      flex: 1,
      backgroundColor: '#F5FCFF',
      padding: 10,
    },
    containerDark: {
      flex: 1,
      backgroundColor: '#CACACA',
      padding: 10,
    },
    title: {
        flex:1,
        color: 'black',
        fontSize: 16,
        fontWeight: 'bold'
    },
    date: {
        fontSize: 12,
        color: '#989898',
    },
    statusRead: {
        color: '#353535',
    },
    statusUnread: {
        color: 'red',
    },
    text: {
        fontSize: 14, 
        color: '#434343',
    },
  });


class HomeScreen extends Component<Props> {
    static navigationOptions = {
        title: 'Home'
    };
    constructor(props) {
        super(props);
    }

    render() {
        let unreadMessage = 0;
        if (this.props.msgData.length > 0) {
            unreadMessage = this.props.msgData.length;
            this.props.msgData.forEach(item => {
                if (item.status) {
                    unreadMessage--;
                }
            });
        }

        return(
            <View
                style={{flex:1}}>
                <Text style={{textAlign: 'center', justifyContent: 'center', alignItems: 'center',padding: 4,}}>Unread Messages: {unreadMessage}</Text>
                <FlatList
                    data={[...this.props.msgData]}
                    renderItem={({item}) => this._renderItem(item)}
                />
            </View>
        );
    }

    _renderItem(item) {
        let status = "unread";
        if (item.status) {
            status = "read";
        }
        let style = styles.containerLight;
        let statusStyle = styles.statusUnread;
        if (item.id % 2 == 0) {
            style = styles.containerDark;
        }
        if (item.status) {
            statusStyle = styles.statusRead;
        }
        return(
            <TouchableWithoutFeedback
            onPress={() => {this._selectItem(item)}}>
                <View style={style}>
                    <View style={{flex:1, flexDirection: 'row'}}>
                        <Text style={styles.title}>{item.title}</Text>
                        <Text style={statusStyle}>{status}</Text>
                    </View>
                    <Text style={styles.date}>{item.date}</Text>
                    <Text style={styles.text} numberOfLines={2} ellipsizeMode={'tail'}>{item.message}</Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    _selectItem(item) {
        this.props.navigation.navigate('Detail', { title: item.title ,item: item });
        this.props.actionSetReadStatus(item);
        this.setState({...this.state});
    }

    componentWillMount() {
        // load local data
        AsyncStorage.getAllKeys((err, keys) => {
            AsyncStorage.multiGet(keys, (err, stores) => {
                let data = stores.map((result, i, store) => {
                    // get at each store's key/value so you can work with it
                    let key = store[i][0];
                    let value = JSON.parse(store[i][1]);

                    return {...value};
                });
                this.props.watchMsgData(data);
            })
        })
    }
}


const mapStateToProps = (state) => {
    return {
        msgData: state.msgData
    };
}


const mapDispatchToProps = (dispatch) => {
    return {
        watchMsgData: (localData) => dispatch(watchMsgData(localData)),
        actionSetReadStatus: (msg, read) => dispatch(actionSetReadStatus(msg, read)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps) (HomeScreen);