import React,  { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { actionSetSelectedItem } from '../../react-redux/app-redux';

const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
    title: {
        padding: 5,
        fontWeight: 'bold',
        fontSize: 24,
        color: 'black',
    },
    date: {
        fontSize: 12,
        padding: 5,
    },
    text: {
        fontSize: 16,
        padding: 5,
    },
})

class MessageDetail extends Component<Props> {
    static navigationOptions = ({navigation}) => ({
        title: `${navigation.state.params.title}`
    });

    render() {
        let msg = "";
        let date = "";
        let title = "";
        if (this.props.selectedItem && this.props.selectedItem.message) {
            msg = this.props.selectedItem.message;
            date = this.props.selectedItem.date;
            title = this.props.selectedItem.title;
        }
        return(
            <View style={styles.container}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.date}>{date}</Text>
                <Text style={styles.text}>{msg}</Text>
            </View>
        );
    }

    componentDidMount() {
        const { item } = this.props.navigation.state.params;
        this._loadItem({item});
    }

    _loadItem(item) {
        this.props.actionSetSelectedItem(item.item);
        this.setState({...this.state})
    }
}

const mapStateToProps = (state) => {
    return {
        selectedItem: state.selectedItem,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actionSetSelectedItem: (item) => dispatch(actionSetSelectedItem(item)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageDetail);