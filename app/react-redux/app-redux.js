import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import ApiKeys from '../config/ApiKeys';
import {
    AsyncStorage,
} from 'react-native';

var firebase;

//
// Initial State
//

const initialState = {
    msgData: {},
    selectedItem: {},
}

//
// Reducer
//

const reducer = ( state = initialState, action ) => {
    switch(action.type) {
        case "setMsgData":
            // save message to database
            action.value.forEach(item => {
                AsyncStorage.setItem(item.key, JSON.stringify(item));
            });
            // sort data
            action.value.sort((a,b) =>{
                let aVal = a.status?1:0;
                let bVal = b.status?1:0;
                return aVal - bVal;
            });
            return { ...state, msgData: action.value};
        case "setReadStatus":
            let msgs = state.msgData;
            for(let i = 0; i < msgs.length; i++) {
                let item = msgs[i];
                if (item.id == action.value.id) {
                    item.status = true;
                    AsyncStorage.setItem(item.key, JSON.stringify(item));
                    break;
                }
            };
            // sort data
            msgs.sort((a,b) =>{
                let aVal = a.status?1:0;
                let bVal = b.status?1:0;
                return aVal - bVal;
            });
            return { ...state, msgData: msgs};
        case "setSelectedItem":
            return { ...state, selectedItem: action.value};
        default:
            return state;
    }
}


//
// Store
//

const store = createStore(reducer, applyMiddleware(thunkMiddleware));
export { store };


//
// Action
//

// Set/watch message
const setMsgData = (msgData) => {
    return {
        type: "setMsgData",
        value: msgData,
    };
};

const watchMsgData = (localData) => {
    return function(dispatch) {
        if (!firebase) {
            firebase = require("firebase");
            firebase.initializeApp(ApiKeys.FirebaseConfig);
        }
        firebase.database().refFromURL("https://rnreduxtest.firebaseio.com/e5d65252-21c8-4030-951f-8a441d886740").on("value",function(snapshot) {
            
            // Do fetch data from firebase
            var msgData = snapshot.val();
            msgData.forEach(element => {
                let localItem = null;
                for(let i = 0; i < localData.length; i++) {
                    if (element.id == localData[i].id) {
                        localItem = localData[i];
                        break;
                    }
                };
                element.key = ""+element.id;
                if (localItem) {
                    element.status = localItem.status;
                } else {
                    element.status = false;
                }
            });

            var actionSetMsgData = setMsgData(msgData);
            dispatch(actionSetMsgData);
        }, function(error) {
            console.log(error);
        });
    }
}

// Set/read status
const setReadStatus = (msg) => {
    return {
        type: "setReadStatus",
        value: msg,
    };
};

const actionSetReadStatus = (msg) => {
    return function(dispatch) {
        let actionSetReadStatus = setReadStatus(msg);
        dispatch(actionSetReadStatus);
    }
}


// set/Load selected item
const setSelectedItem = (item) => {
    return {
        type: "setSelectedItem",
        value: item
    }
}

const actionSetSelectedItem = (item) => {
    return function(dispatch) {
        let action = setSelectedItem(item);
        dispatch(action);
    }
}

export { setMsgData, watchMsgData, setReadStatus, actionSetReadStatus, setSelectedItem, actionSetSelectedItem };