import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { store } from './react-redux/app-redux';
import { HomeScreen, MessageDetail } from './screens/index';
import { StackNavigator } from 'react-navigation';

let config = {
    apiKey: "AIzaSyA1j4DuJ8tBkWq0BnMgw3ibQlR-5-Jqhyk",
    authDomain: "just-test-f1799.firebaseapp.com",
    databaseURL: "https://just-test-f1799.firebaseio.com",
    projectId: "just-test-f1799",
    storageBucket: "just-test-f1799.appspot.com",
    messagingSenderId: "679873275964"
};

const Stack = StackNavigator({
    Home: {
      screen: HomeScreen
    },
    Detail: {
      screen: MessageDetail
    }
  });

export default class App extends Component<Props> {

    constructor(props) {
        super(props);

        // initialize firebase
        // Firebase.initializeApp(config);
    }


    render() {
        return(
            <Provider store={store}>
                <Stack />
            </Provider>
        );
    }

    componentWillMount() {
    }
}