import Firebase from 'firebase';

let config = {
    apiKey: "AIzaSyA1j4DuJ8tBkWq0BnMgw3ibQlR-5-Jqhyk",
    authDomain: "just-test-f1799.firebaseapp.com",
    databaseURL: "https://just-test-f1799.firebaseio.com",
    projectId: "just-test-f1799",
    storageBucket: "just-test-f1799.appspot.com",
    messagingSenderId: "679873275964"
};

let firebaseApp = Firebase.initializeApp(config);
export default firebaseApp;